var express = require('express');
var router = express.Router();
const ct = require("../controllers/controllers")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// Ruta para mostrar la lista de personas
router.get('/personas', ct.listaPersonas);

// Ruta para mostrar el formulario de agregar registros
router.get('/agregar', ct.getAgregarPersonas);

// Ruta para procesar el formulario de agregar registros
router.post('/agregar', ct.postAgregarPersonas);

// Ruta para mostrar el formulario de modificar registros
router.get('/modificar/:id', ct.getModificarPersonas);

// Ruta para procesar el formulario de modificar registros
router.post('/modificar/:id', ct.postModificarPersonas);

// Ruta para mostrar la confirmación de borrado
router.get('/borrar/:id/confirmacion', ct.getBorrarPersona);

// Ruta para borrar registros (procesar el borrado)
router.post('/borrar/:id', ct.postBorrarPersona);

//Ruta para mostrar formulario de busqueda
router.get('/buscar', ct.buscarPersona);

//Ruta para mostrar resultados de la busqueda
router.post('/buscar/resultados', ct.buscarPersonaResultados);



module.exports = router;
