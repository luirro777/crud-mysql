const mysql = require('mysql2');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '1234',
});

// Conectar a MySQL
db.connect((err) => {
    if (err) {
        console.error('Error al conectar a MySQL:', err);
        return;
    }

    // Verificar si la base de datos existe
    db.query("CREATE DATABASE IF NOT EXISTS crud", (err) => {
        if (err) {
            console.error('Error al crear la base de datos o ya existe:', err);
            return;
        }
        console.log('Base de datos creada o ya existente');
    });

    // Seleccionar la base de datos recién creada o existente
    db.query('USE crud', (err) => {
        if (err) {
            console.error('Error al seleccionar la base de datos:', err);
            return;
        }
        console.log('Conexión a la base de datos establecida');
    });

    // Verificar si la tabla "personas" existe y crearla si es necesario
    const createTableSQL = `
      CREATE TABLE IF NOT EXISTS personas (
        id INT AUTO_INCREMENT PRIMARY KEY,
        nombre VARCHAR(255),
        email VARCHAR(255)      
      )
    `;

    db.query(createTableSQL, (err) => {
        if (err) {
            console.error('Error al crear la tabla "personas" o ya existe:', err);
            return;
        }
    });
});

module.exports = db;
