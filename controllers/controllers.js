const listaPersonas = (req, res) => {
    db = req.app.get("db");
    //console.log(db);
    db.query('SELECT * FROM personas', (error, resultados) => {
      if (error) {
        console.error(error);
        res.status(500).send('Error en la consulta a la base de datos');
      } else {
        res.render('listar', { registros: resultados });
      }
    });
}

const getAgregarPersonas = (req, res) => {
    res.render('agregar');
}

const postAgregarPersonas = (req, res) => {
    db = req.app.get("db");
    // Recoge los datos enviados desde el formulario
    const { nombre, email } = req.body;
    console.log(req.body);
  
    // Realiza una inserción en la base de datos con los datos recogidos
    db.query('INSERT INTO personas (nombre, email) VALUES (?, ?)', [nombre, email], (error, resultado) => {
      if (error) {
        console.error(error);
        res.status(500).send('Error al agregar el registro');
      } else {
        // Redirige a la página de lista de registros o muestra un mensaje de éxito
        res.redirect('/personas');
      }
    });
}

const getModificarPersonas = (req, res) => {
    db = req.app.get("db");
    const id = req.params.id;
    // Realiza una consulta para obtener el registro a modificar por su ID
    db.query('SELECT * FROM personas WHERE id = ?', [id], (error, resultado) => {
      if (error) {
        console.error(error);
        res.status(500).send('Error al cargar el formulario de modificación');
      } else {
        res.render('modificar', { registro: resultado[0] });
      }
    });
}

const postModificarPersonas = (req, res) => {
    db = req.app.get("db");
    const id = req.params.id;
    // Procesa los datos del formulario de modificación y actualiza el registro en la base de datos
    const { nombre, email } = req.body;
    db.query('UPDATE personas SET nombre = ?, email = ? WHERE id = ?', [nombre, email, id], (error, resultado) => {
      if (error) {
        console.error(error);
        res.status(500).send('Error al modificar el registro');
      } else {
        // Redirige a la página de lista de registros o muestra un mensaje de éxito
        res.redirect('/listar');
      }
    });
}

const getBorrarPersona = (req, res) => {
    db = req.app.get("db");
    const id = req.params.id;
    // Realiza una consulta para obtener los detalles del registro antes de mostrar la confirmación
    db.query('SELECT * FROM personas WHERE id = ?', [id], (error, resultado) => {
      if (error) {
        console.error(error);
        res.status(500).send('Error al cargar la confirmación de borrado');
      } else {
        res.render('confirmarBorrado', { registro: resultado[0] });
      }
    });
}

const postBorrarPersona = (req, res) => {
    db = req.app.get("db");
    const id = req.params.id;
    // Realiza una consulta para eliminar el registro por su ID
    db.query('DELETE FROM personas WHERE id = ?', [id], (error, resultado) => {
      if (error) {
        console.error(error);
        res.status(500).send('Error al borrar el registro');
      } else {
        // Redirige a la página de lista de registros o muestra un mensaje de éxito
        res.redirect('/personas');
      }
    });
}

const buscarPersona = (req, res, next) => {
    res.render('busqueda', {});
}
  
const buscarPersonaResultados = (req, res, next) => {
    const db = req.app.get("db");
    const keyword = req.body.keyword;
    //console.log(req.query);
    //console.log(db);
    const query = 'SELECT * FROM personas WHERE nombre LIKE ?';
    db.query(query, [`%${keyword}%`], (err, rows) => {
      if (err) throw err;
      console.log(rows);
      res.render('resultados', {people:rows});
    });
}
  

module.exports = {
    listaPersonas,
    getAgregarPersonas,
    postAgregarPersonas,
    getModificarPersonas,
    postModificarPersonas,
    getBorrarPersona,
    postBorrarPersona,
    buscarPersona,
    buscarPersonaResultados
};


